package main

import (
	"crypto/sha1"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type clientResolver interface {
	mayAllowToSendToken(clientID string) bool
	generatePrivateKey(eventID, token string) []byte
}

type hlsClientResolver struct {
	seed string
}

func (r hlsClientResolver) mayAllowToSendToken(clientID string) bool {
	doesClientExistAndHasPermission := func() bool {
		n := len(clientID)
		return 1 <= n && n <= 64 // some logic...
	}
	return doesClientExistAndHasPermission()
}
func (r hlsClientResolver) generatePrivateKey(eventID, token string) []byte {
	h := sha1.New() // TODO: use appropriate hash function for hls
	h.Write([]byte(r.seed + "_" + eventID + "_" + token))
	return h.Sum(nil)
}

// --------------------------------------

func newClientResolver() (clientResolver, error) {
	seed := "some seed that may be extracted from config" // from config.seed
	return hlsClientResolver{seed}, nil                   // if config.type == "hls"
}

// --------------------------------------

func hlsHandler() func(w http.ResponseWriter, eventID, clientID, token string) {
	resolver, err := newClientResolver()
	if err != nil {
		log.Fatalln(err)
	}

	return func(w http.ResponseWriter, eventID, clientID, token string) {
		if !resolver.mayAllowToSendToken(clientID) || len(eventID) == 0 {
			w.WriteHeader(404)
			return
		}
		privateKey := resolver.generatePrivateKey(eventID, token)
		fmt.Fprintf(w, "%x\n", privateKey)
	}
}

func main() {
	handle := hlsHandler()
	router := mux.NewRouter()
	router.HandleFunc("/generate-hls", func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()
		eventID := query.Get("eventID")
		clientID := query.Get("clientID")
		token := query.Get("token")

		handle(w, eventID, clientID, token)
	})
	log.Fatal(http.ListenAndServe(":8080", router))
}

/*
Необходимо написать следующее приложение, реализующее управление ключами шифрования hls

Клиенты обращаются по заданному в настройках формату url за ключом доступа. формат юрл содержит ид эвента ид клиента и имя ключа, представляющее собой зашифрованный токен.
Ключ, отдаваемый по этому запросу генерируется как хеш ид эвента, ид  ключа и некоторой соли.
Приложение должно реализовать интерфейс для получения разрешения передать ключ указанному клиенту.
Приложение должно отклонять запросы, если запрос не соответствует формату запроса или  имя ключа (токен) не корректный.
Приложение должно реализовывать метод апи для генерации ключа по указанному ид эвента
Приложение не должно использовать какие-либо бд.
*/
